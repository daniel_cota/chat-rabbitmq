# Actividad | Implementación de la arquitectura de un chat
***
Utilizando el conocimiento adquirido en clase vamos a crear una sala de chat. el chat room es de solo 2 participantes con la opción de crear varias conexiones donde el nombre de la cola será el nombre de las dos personas que estén hablando entre si, como entregable se espera el código fuente del proyecto en git y en el archivo readme.md las instrucciones para hacerlo funcionar.

## Equipo: ***Machín Learning*** :floppy_disk:

## Profesor: I.S.C. Luis Antonio Ramirez Martinez

### Instrucciones
1. Primero clonamos el repo y hacemos el npm install.
```bash
$ git clone https://gitlab.com/daniel_cota/chat-rabbitmq
$ cd ./chat-rabbitmq
$ npm install
```

2. Luego lanzamos la imagen de docker para localizar el puerto.
```bash
$ docker run -dti -P --hostname=rabbit --name=rabbit -e RABBITMQ_DEFAULT_USER=<user> -e RABBITMQ_DEFAULT_PASS=<password> rabbitmq:3-management
```

3. Una vez la imagen este corriendo, localizamos el puerto ligado al 5672 y lo colocamos en la variable "port" de nuestro archivo chat.js.

4. A su vez modificamos las variables "user" y "password" por las que declaramos al momento de lanzar la imagen.

5. Modificamos el p1 y p2 dependiendo de las personas que vayan a interactuar en la sala.

6. Corremos el archivo chat.js por medio de node
```bash
$ node chat.js
```

7. Recomendación: Correr el codigo del punto 6 en dos consolas separadas y empezar la conversación en la segunda consola para una mejor experiencia.

## Proyecto
- [Principal](https://gitlab.com/daniel_cota/chat-rabbitmq)

## Autores

- Daniel Alberto Cota Ochoa     ***329701***
    - [GitLab](https://gitlab.com/a329701)
- Axel Dalí Gomez Morales       ***329881***
    - [GitLab](https://gitlab.com/axel_dali)
