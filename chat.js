const amqp = require('amqplib/callback_api');
const readline = require("readline");
const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/Respaldo", {useNewUrlParser:true, useUnifiedTopology:true} ,(err)=>{
    if(!err) console.log('Conexión con la base de datos');
    else console.log('ERROR');
})

const NewSchema = new mongoose.Schema({
    message: String
});

const newModel = new mongoose.model("Collection", NewSchema);

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const user = "Ayuda";
const password = "ayuda1234567";
const host = "b-04fdf4f9-de7e-49c9-bb0b-e1ae018b901f.mq.us-east-1.amazonaws.com";
const port = "5671";
const p1 = "Dr. Strange";
const p2 = "Scarlet Witch";

amqp.connect(`amqps://${user}:${password}@${host}:${port}`, (err, con) =>{
    
    if(err){
        throw err;
    }

    con.createChannel((err1, channel)=> {
        
        if(err1){
            throw err1;
        }

        let queue = `${p2}-${p1}`;

        channel.assertQueue(queue, {
            durable: false
        });

        console.log("Esperando mensajes del servidor");

        channel.consume(queue, (message)=> {
            console.log(message.content.toString() + "\n");
            const data = new newModel({message:`${message.content.toString()}`});
            data.save();
        }, { noAck: true});

    });

    rl.on('line', (msg) => {
        con.createChannel((err2, channel)=> {
        
            if(err2){
                throw err2;
            }
       
            let message = `${p1}--> ${msg}`;
            let queue = `${p1}-${p2}`;
            const data = new newModel({message:`${message}`});
            data.save();
       
            channel.assertQueue(queue, {
                durable: false
            });
        
            channel.sendToQueue(queue, Buffer.from(message));
        
        });
    });
    
});
