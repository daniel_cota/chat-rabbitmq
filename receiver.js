const amqp = require('amqplib/callback_api');

amqp.connect("amqp://Dali:abc1234@localhost:49164", (err, con)=>{
    
    if(err){
        throw err;
    }

    con.createChannel((err1, channel)=> {
        
        if(err1){
            throw err1;
        }

        let queue = "mensajesDali";

        channel.assertQueue(queue, {
            durable: false
        });

        console.log("Esperando mensajes del servidor");

        channel.consume(queue, (message)=> {
            console.log("->" + message.content.toString());
        }, { noAck: true});

    });

});