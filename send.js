const amqp = require('amqplib/callback_api');

// amqp://<user>?:?<password>?@?<host>:<port>
amqp.connect("amqp://Dali:abc1234@localhost:49164", (err, con)=>{
    
    if(err){
        throw err;
    }

    con.createChannel((err1, channel)=> {
        
        if(err1){
            throw err1;
        }

        let message = "Hola RabbitMQ";
        let queue = "mensajesDali";

        channel.assertQueue(queue, {
            durable: false
        });

        channel.sendToQueue(queue, Buffer.from(message));
        console.log("Mensaje enviado");

    });

    setTimeout(()=>{
        con.close();
        process.exit(0);
    }, 500);
    
});